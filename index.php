<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
$GLOBALS['indexLink'] = "index.php";
$GLOBALS['logo'] = "logo.png";
require('client.inc.php');
$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');

$_SESSION['design'] = "";
?>

<?php
// uncomment ici pour revenir à l'original
//$eteind = '';
//if($_REQUEST["caroussel"] == "1"){
//    $eteind = "switchOff";
//}

// pour éteindre toutes les parties indésirables,
// on fait de la place pour le caroussel
$eteind = "switchOff";
?>

<div id="landing_page" class="<?php echo $eteind; ?>">
    <?php
    if($cfg && ($page = $cfg->getLandingPage()))
        echo $page->getBodyWithImages();
    else
        echo  '<h1 class="'.$eteind.'">'.__('Welcome to the Support Center').'</h1>';
    ?>
    <div id="new_ticket" class="pull-left">
        <h3><?php echo __('Open a New Ticket');?></h3>
        <br>
        <div><?php echo __('Please provide as much detail as possible so we can best assist you. To update a previously submitted ticket, please login.');?></div>
    </div>

    <div id="check_status" class="pull-right">
        <h3><?php echo __('Check Ticket Status');?></h3>
        <br>
        <div><?php echo __('We provide archives and history of all your current and past support requests complete with responses.');?></div>
    </div>

    <div class="clear"></div>
    <div class="front-page-button pull-left">
        <p>
            <a href="open.php" class="green button"><?php echo __('Open a New Ticket');?></a>
        </p>
    </div>
    <div class="front-page-button pull-right <?php echo $eteind; ?>">
        <p>
            <a href="view.php" class="blue button"><?php echo __('Check Ticket Status');?></a>
        </p>
    </div>
</div>
<div class="clear"></div>

<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p class="<?php echo $eteind; ?>"><?php echo sprintf(
    __('Be sure to browse our %s before opening a ticket'),
    sprintf('<a href="kb/index.php">%s</a>',
        __('Frequently Asked Questions (FAQs)')
    )); ?></p>

<?php
if($_REQUEST["caroussel"] != ""){
?>
    <p id="optimis_link" class="<?php echo $eteind; ?>">plus d'informations sur notre site:
        <a href="http://www.groupeoptimis.fr/vdevsite/" target="_blank">www.groupeoptimis.fr</a>
    </p>
<?php
}
?>




<?php
// uncomment ici pour revenir à l'original
//if($_REQUEST["caroussel"] == 1){
?>
    <div class="clear"></div>
    <div class="front-page-button pull-left">
        <p>
            <a href="open.php" class="green button"><?php echo __('Open a New Ticket');?></a>
        </p>
    </div>
    <div class="clear"></div>

<?php if($_SESSION['design'] == 'avea'){ ?>
<div id="container_caroussel">
    <div class="your-class">
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/buzi.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/avea.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/chaine_valeur_erp.png'; ?>"/></div>
    </div>
</div>
<?php }else{ ?>
<div id="container_caroussel">
    <div class="your-class">
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/oemail.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/opicking.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/otrm.png'; ?>"/></div>
    </div>
</div>
<?php } ?>

<?php
// uncomment ici pour revenir à l'original
//}
?>

</div>

<?php
} ?>
<?php
require(CLIENTINC_DIR.'footer.inc.php');
?>
