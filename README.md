# README #

Project Optimis OsTicket : Upgrading the website.

by NGUYEN Le Hoang Dinh

### This project contains ###

* The last version of the website support
* Modifications made by me ordered by Frederic
* Each commit is numbered and means/contains one modified successfully.

### About the website ###

It made to help clients to fix their problems, they have to create a ticket then they will be contacted by the staff.
After fixing the problem, the ticket will be closed, and every ticket are saved in the database to keep traces.

### How do I work ? ###

* Reading existing codes (mostly uncommented)
* understand it
* add some lines or modify some lines to reach to wanted goal.

### My opinion ###

* working to an existing code is really challenging but it's hard solve issues.
* It made me realize how important are commentaries.
