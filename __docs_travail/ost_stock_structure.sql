CREATE TABLE `ost_stock_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `quantite` int(11) NOT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='gestion du stock de tickets par organisation (champs org_id)\nquantité positive -> acheté par l''organisation correspondante\nquantité négative -> dépensé par l''organisation';
