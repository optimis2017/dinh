<?php
/*********************************************************************
    logo.php

    Simple logo to facilitate serving a customized client-side logo from
    osTicet. The logo is configurable in Admin Panel -> Settings -> Pages

    Peter Rotich <peter@osticket.com>
    Jared Hancock <jared@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/

// Don't update the session for inline image fetches
if (!function_exists('noop')) { function noop() {} }
session_set_save_handler('noop','noop','noop','noop','noop','noop');
define('DISABLE_SESSION', true);

require('client.inc.php');



// ANCIEN: 
// if (($logo = $ost->getConfig()->getClientLogo())) {
// {
//     $logo->display();
// } else {
//     header('Location: '.ASSETS_PATH.'images/logo.png');
// }


$logo_perso = $_SESSION['design'];


if (($logo = $ost->getConfig()->getClientLogo())) {

   
    header('Location: images/logo'.$logo_perso.'.png');

     // header('Location: '.ASSETS_PATH.'images/logo'.$logo_perso.'.png');


} else
{
    //$logo->display();
}

?>
