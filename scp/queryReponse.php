<?php

    define('DBHOST','localhost');
    define('DBNAME','optimis');
    define('DBUSER','root');
    define('DBPASS','0000');
    // define('DBHOST','mysql55-217.bdb');
    // define('DBNAME','groupeopwticket');
    // define('DBUSER','groupeopwticket');
    // define('DBPASS','T1ck3tss');
    $pdo = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME,DBUSER,DBPASS);
    $myQuery = "SELECT datediff(ticket.closed, ticket.created) as deltaJours, count(datediff(ticket.closed, ticket.created)) as nbTickets "
    ." from ost_ticket ticket"
    ." WHERE user_id in (select id from ost_user where org_id = ".$_REQUEST['id'].")"
    ." and status_id = 3"
    ." AND year(ticket.created) = ".$_REQUEST['year']
    ." group by datediff(ticket.closed, ticket.created)";
    $statement = $pdo->query($myQuery);
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);
    header("content-type:application/json");
    echo json_encode($row);
    exit();
