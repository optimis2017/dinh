<?php
class staffConditions {

    public $userTeams;

    public function __construct($arrayTeams) {
        $this->userTeams = $arrayTeams;
    }

//////////////////////////////////////////////////////////////////
//          ajout d'une condition pour n'afficher
//          que les organisations qui ont la meme
//          team que celle(s) de l'agent
//////////////////////////////////////////////////////////////////
    public function AndWithTeams($organizationAlias = 'org', $arrayTeams = array()) {

        $andStaffCondition = '';
        $tempAndStaffCondition = '';
        foreach ($this->userTeams as $value) {
            $isOr = 'OR';
            if(empty($tempAndStaffCondition))
                    $isOr = '';
            $tempAndStaffCondition .= sprintf(' %s SUBSTR(%s.manager, 2) = "%s" ', $isOr, $organizationAlias, $value);
        }
        if(!empty($tempAndStaffCondition))
            $andStaffCondition = sprintf(' AND (%s OR %s.manager="") ', $tempAndStaffCondition, $organizationAlias);

        return $andStaffCondition;
    }

        public function AndWithTicketsTeams($arrayTeams = array()) {

        $andStaffCondition = '';
        $tempAndStaffCondition = '';
        foreach ($this->userTeams as $value) {
            $isOr = 'OR';
            if(empty($tempAndStaffCondition))
                    $isOr = '';
            $tempAndStaffCondition .= sprintf(" %s ticket.team_id = '%s' ", $isOr, $value);
        }
        if(!empty($tempAndStaffCondition))
            $andStaffCondition = sprintf('(%s)', $tempAndStaffCondition);

        return $andStaffCondition;
    }

    public function feedUserTeam($arrayTeams) {
        if (count( $arrayTeams ) > 0) {
            $this->userTeams = $arrayTeams;
        }
    }

}
