<?php
include_once(STAFFINC_DIR.'staffConditions.php');

if(!defined('OSTSCPINC') || !$thisstaff) die('Access Denied');

$qs = array();

$select = 'SELECT org.*
            ,COALESCE(team.name,
                    IF(staff.staff_id, CONCAT_WS(" ", staff.firstname, staff.lastname), NULL)
                    ) as account_manager ';
$from = 'FROM '.ORGANIZATION_TABLE.' org '
       .'LEFT JOIN '.STAFF_TABLE.' staff ON (
           LEFT(org.manager, 1) = "s" AND staff.staff_id = SUBSTR(org.manager, 2)) '
       .'LEFT JOIN '.TEAM_TABLE.' team ON (
           LEFT(org.manager, 1) = "t" AND team.team_id = SUBSTR(org.manager, 2)) ';

$where = " WHERE 1 ";
// $where = " WHERE 1 ";

if ($_REQUEST['query']) {

    $from .=' LEFT JOIN '.FORM_ENTRY_TABLE.' entry
                ON (entry.object_type=\'O\' AND entry.object_id = org.id)
              LEFT JOIN '.FORM_ANSWER_TABLE.' value
                ON (value.entry_id=entry.id) ';

    $search = db_input(strtolower($_REQUEST['query']), false);
    $where .= ' AND (
                    org.name LIKE \'%'.$search.'%\' OR value.value LIKE \'%'.$search.'%\'
                ) ';

    $qs += array('query' => $_REQUEST['query']);
}

////////////////////////////////////////////////////////////////////
////          ajout d'une condition pour n'afficher
////          que les organisations qui ont la meme
////          team que celle(s) de l'agent
////////////////////////////////////////////////////////////////////
//$andStaffCondition = '';
//$tempAndStaffCondition = '';
//foreach ($thisstaff->teams as $value) {
//    $isOr = 'OR';
//    if(empty($tempAndStaffCondition))
//            $isOr = '';
//    $tempAndStaffCondition .= sprintf(" %s SUBSTR(org.manager, 2) = '%s' ", $isOr, $value);
//}
//if(!empty($tempAndStaffCondition))
//    $andStaffCondition = sprintf(' and (%s) ', $tempAndStaffCondition);


$sortOptions = array('name' => 'org.name',
                     'users' => 'users',
                     'tickets' => 'stock_tickets',
                     'contract' => 'contract',
                     'tel' => 'tel',
                     'gest' => 'org.manager',
                     'version' => 'version');
$orderWays = array('DESC'=>'DESC','ASC'=>'ASC');
$sort= ($_REQUEST['sort'] && $sortOptions[strtolower($_REQUEST['sort'])]) ? strtolower($_REQUEST['sort']) : 'name';
//Sorting options...
if ($sort && $sortOptions[$sort])
    $order_column =$sortOptions[$sort];

$order_column = $order_column ?: 'org.name';

if ($_REQUEST['order'] && $orderWays[strtoupper($_REQUEST['order'])])
    $order = $orderWays[strtoupper($_REQUEST['order'])];

$order=$order ?: 'ASC';
if ($order_column && strpos($order_column,','))
    $order_column = str_replace(','," $order,",$order_column);

$x=$sort.'_sort';
$$x=' class="'.strtolower($order).'" ';
$order_by="$order_column $order ";

$total=db_count('SELECT count(DISTINCT org.id) '.$from.' '.$where);
$page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
$pageNav=new Pagenate($total,$page,PAGE_LIMIT);
$qstr = '&amp;'. Http::build_query($qs);
$qs += array('sort' => $_REQUEST['sort'], 'order' => $_REQUEST['order']);
$pageNav->setURL('orgs.php', $qs);
$qstr.='&amp;order='.($order=='DESC' ? 'ASC' : 'DESC');

$select .= ', count(DISTINCT user.id) as users ';
$select .= ', (SELECT sum(total_tickets.quantite) FROM '.STOCK_TICKET_TABLE.' total_tickets WHERE total_tickets.org_id = org.id) as stock_tickets ';
$select .= ', entry_values_contrat.value as contract, entry_values_tel.value as tel, entry_values_version.value as version,man.name as gestionnaire ';

$from .= ' LEFT JOIN '.USER_TABLE.' user ON (user.org_id = org.id) ';
$from .= "left join ost_form_entry as form_entry
on form_entry.object_id = org.id

left join ost_form_entry_values as entry_values_tel
on entry_values_tel.entry_id = form_entry.id
and entry_values_tel.field_id = '29'

left join ost_form_entry_values as entry_values_contrat
on entry_values_contrat.entry_id = form_entry.id
and entry_values_contrat.field_id = '34'

left join ost_form_entry_values as entry_values_version
on entry_values_version.entry_id = form_entry.id
and entry_values_version.field_id = '36'

LEFT JOIN ost_team man
on org.manager = concat('t',man.team_id)";
//$from .= ' LEFT JOIN '.STOCK_TICKET_TABLE.' total_tickets ON (total_tickets.org_id = org.id) ';


$staffCondition = new staffConditions($thisstaff->teams);

$query="$select $from $where ".$staffCondition->AndWithTeams()." AND form_entry.object_type = 'o' GROUP BY org.id ORDER BY $order_by LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();

//echo '<pre>';
////print_r($thisstaff->ht['group_name']);
////print_r($tempAndStaffCondition);
//print_r($query);
//echo '</pre>';


$qhash = md5($query);
$_SESSION['orgs_qs_'.$qhash] = $query;
?>
<h2><?php echo __('Organizations'); ?></h2>
<div class="pull-left" style="width:700px;">
    <form action="orgs.php" method="get">
        <?php csrf_token(); ?>
        <input type="hidden" name="a" value="search">
        <table>
            <tr>
                <td><input type="text" id="basic-org-search" name="query" size=30 value="<?php echo Format::htmlchars($_REQUEST['query']); ?>"
                autocomplete="off" autocorrect="off" autocapitalize="off"></td>
                <td><input type="submit" name="basic_search" class="button" value="<?php echo __('Search'); ?>"></td>
                <!-- <td>&nbsp;&nbsp;<a href="" id="advanced-user-search">[advanced]</a></td> -->
            </tr>
        </table>
    </form>
 </div>
 <div class="pull-right flush-right">
    <b><a href="#orgs/add" class="Icon newDepartment add-org"><?php
    echo __('Add New Organization'); ?></a></b></div>
<div class="clear"></div>
<?php
$showing = $search ? __('Search Results').': ' : '';
$sql="SELECT org.*,count(org.id) AS counter,
                COALESCE(team.name,
                    IF(staff.staff_id, CONCAT_WS(' ', staff.firstname, staff.lastname), NULL)
                    ) as account_manager FROM ".ORGANIZATION_TABLE." org
       LEFT JOIN ".STAFF_TABLE." staff ON (
           LEFT(org.manager, 1) = 's' AND staff.staff_id = SUBSTR(org.manager, 2))
       LEFT JOIN ".TEAM_TABLE." team ON (
           LEFT(org.manager, 1) = 't' AND team.team_id = SUBSTR(org.manager, 2))
           where org.manager like 't%'";
$res = db_query($query);
$res2 = db_query($sql);
$row = db_fetch_array($res2);
session_start();
if (!isset($_SESSION['count'])) {
  $_SESSION['count']=$row['counter'];
}else {
  unset($_SESSION['count']);
  $_SESSION['count']=$row['counter'];
}
if($res && ($num=db_num_rows($res)))
  $showing .= $pageNav->showing();
else
  $showing .= __('No organizations found!');
?>
<form action="orgs.php" method="POST" name="staff" >
 <?php csrf_token(); ?>
 <input type="hidden" name="do" value="mass_process" >
 <input type="hidden" id="action" name="a" value="" >
 <table class="list" border="0" cellspacing="1" cellpadding="0" width="940">
    <caption><?php echo $showing; ?></caption>
    <thead>
        <tr>
            <th width="500"><a <?php echo $name_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=name"><?php echo __('Name'); ?></a></th>
            <th width="200"><a <?php echo $gest_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=gest"><?php echo __('Gestionnaire'); ?></a></th>
            <th width="120"><a <?php echo $tma_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=tickets"><?php echo __('T.M.A.'); ?></a></th>
            <th width="70"><a <?php echo $users_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=users"><?php echo __('Users'); ?></a></th>
            <th width="170"><a <?php echo $contract_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=contract"><?php echo __('Contrat'); ?></a></th>
            <th width="90"><a <?php echo $tel_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=tel"><?php echo __('Téléphone'); ?></a></th>
            <th width="70"><a <?php echo $version_sort; ?> href="orgs.php?<?php echo $qstr; ?>&sort=version"><?php echo __('Version PMI'); ?></a></th>
        </tr>
    </thead>
    <tbody>
    <?php
        if($res && db_num_rows($res)):
            $ids=($errors && is_array($_POST['ids']))?$_POST['ids']:null;
            while ($row = db_fetch_array($res)) {

                $sel=false;
                if($ids && in_array($row['id'], $ids))
                    $sel=true;
                ?>
               <tr id="<?php echo $row['id']; ?>">
                <td>&nbsp; <a href="orgs.php?id=<?php echo $row['id']; ?>"><?php echo $row['name']; ?></a> </td>
                <td>&nbsp;<?php echo $row['gestionnaire']; ?></td>
                <?php
                  if ($row['stock_tickets'] < 0) {
                    ?>
                    <td style="color : red">&nbsp;<?php echo $row['stock_tickets']; ?></td>
                    <?php } else {
                      ?>
                      <td >&nbsp;<?php echo $row['stock_tickets']; ?></td>
                      <?php
                      }
                      ?>
                <td>&nbsp;<?php echo $row['users']; ?></td>
                <?php
                 $tabContrat = explode("\"", $row['contract']);
                 if (strlen($row['contract'])<1) {
                  ?>
                   <td style="color:#84afd6">Non attribué</td>
                   <?php }else {?>
                     <td><?php echo $tabContrat[3] ?></td>
                     <?php }
                     if (strlen($row['tel'])<1) {
                       ?>
                        <td style="color:#84afd6">Non attribué</td>
                        <?php }else {?>
                          <td><?php echo $row['tel'] ?></td>
                          <?php }
                 $tabVersion = explode("\"", $row['version']);
                 if (strlen($row['version'])<1) {
                   ?>
                    <td style="color:#84afd6">Non attribuée</td>
                    <?php }else {?>
                      <td><?php echo $tabVersion[3]; ?></td>
                      <?php } ?>
               </tr>
            <?php
            } //end of while.
        endif; ?>
    </tbody>
</table>
<?php
if($res && $num): //Show options..
    echo sprintf('<div>&nbsp;%s: %s &nbsp; <a class="no-pjax"
            href="orgs.php?a=export&qh=%s">%s</a></div>',
            __('Page'),
            $pageNav->getPageLinks(),
            $qhash,
            __('Export'));
endif;
?>
</form>

<script type="text/javascript">
$(function() {
    $('input#basic-org-search').typeahead({
        source: function (typeahead, query) {
            $.ajax({
                url: "ajax.php/orgs/search?q="+query,
                dataType: 'json',
                success: function (data) {
                    typeahead.process(data);
                }
            });
        },
        onselect: function (obj) {
            window.location.href = 'orgs.php?id='+obj.id;
        },
        property: "/bin/true"
    });

    $(document).on('click', 'a.add-org', function(e) {
        e.preventDefault();
        $.orgLookup('ajax.php/orgs/add', function (org) {
            window.location.href = 'orgs.php?id='+org.id;
         });

        return false;
     });
});
</script>
