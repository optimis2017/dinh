<?php
if(!defined('OSTSCPINC') || !$thisstaff || !is_object($org)) die('Invalid path');

//print_r(STAFFINC_DIR);

?>
<head>
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload=
    {'modules':[{'name':'visualization','version':'1.1','packages':
    ['corechart','gauge']}]}"></script>
</head>
<table width="940" cellpadding="2" cellspacing="0" border="0">
    <tr>
        <td width="50%" class="has_bottom_border">
             <h2><a href="orgs.php?id=<?php echo $org->getId(); ?>"
             title="Reload"><i class="icon-refresh"></i> <?php echo $org->getName(); ?></a></h2>
        </td>
        <td width="50%" class="right_align has_bottom_border">
            <span class="action-button pull-right" data-dropdown="#action-dropdown-more">
                <i class="icon-caret-down pull-right"></i>
                <span ><i class="icon-cog"></i> <?php echo __('More'); ?></span>
            </span>
            <a id="org-delete" class="action-button pull-right org-action"
            href="#orgs/<?php echo $org->getId(); ?>/delete"><i class="icon-trash"></i>
            <?php echo __('Delete Organization'); ?></a>
            <div id="action-dropdown-more" class="action-dropdown anchor-right">
              <ul>
                <li><a href="#ajax.php/orgs/<?php echo $org->getId();
                    ?>/forms/manage" onclick="javascript:
                    $.dialog($(this).attr('href').substr(1), 201);
                    return false"
                    ><i class="icon-paste"></i>
                    <?php echo __('Manage Forms'); ?></a></li>
              </ul>
            </div>
        </td>
    </tr>
</table>
<table class="ticket_info" cellspacing="0" cellpadding="0" width="940" border="0">
    <tr>
        <td width="50%">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                     <th width="150"><?php echo __('Name'); ?>:</th>
                     <td><b><a href="#orgs/<?php echo $org->getId();
                     ?>/edit" class="org-action"><i
                     class="icon-edit"></i>&nbsp;<?php echo
                     $org->getName();
                     ?></a></td>
                 </tr>
                <tr>
                    <th><?php echo __('Account Manager'); ?>:</th>
                    <td><?php echo $org->getAccountManager(); ?>&nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="50%" style="vertical-align:top">
            <table border="0" cellspacing="" cellpadding="4" width="100%">
                <tr>
                    <th width="150"><?php echo __('Created'); ?>:</th>
                    <td><?php echo Format::db_datetime($org->getCreateDate()); ?></td>
                </tr>
                <tr>
                    <th><?php echo __('Last Updated'); ?>:</th>
                    <td><?php echo Format::db_datetime($org->getUpdateDate()); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<div class="clear"></div>
<?php
// pour gérer le défilement des pages dans l'onglet "tickets payants"
$class_u = 'active';
$class_tp = '';
$style_u = 'display:block;';
$style_tp = 'display:none;';
if($_REQUEST['tp'] == '1'){ // si l'on switche entre les différentes pages des "onglets tickets payants"
   $class_u = '';
   $class_tp = 'active';
   $style_u = 'display:none;';
   $style_tp = 'display:block;';
}
?>
<ul class="tabs">
    <li><a class="<?php echo $class_u; ?>" id="users_tab" href="#users"><i
    class="icon-user"></i>&nbsp;<?php echo __('Users'); ?></a></li>
    <li><a id="tickets_tab" href="#tickets"><i
    class="icon-list-alt"></i>&nbsp;<?php echo __('Tickets'); ?></a></li>
    <li><a id="notes_tab" href="#notes"><i
    class="icon-pushpin"></i>&nbsp;<?php echo __('Notes'); ?></a></li>
    <li><a class="<?php echo $class_tp; ?>" id="tma_tab" href="#visuStockTickets"><i
    class="icon-tags"></i>&nbsp;<?php echo __('T.M.A'); ?></a></li>
    <li><a id="reporting_tab" href="#reporting"><i></i>&nbsp;&nbsp;&nbsp;<?php echo __('REPORTING'); ?></a></li>
</ul>
<div class="tab_content" id="users" style="<?php echo $style_u; ?>">
<?php
include STAFFINC_DIR . 'templates/users.tmpl.php';
?>
</div>

<div class="tab_content" id="tickets"  style="display:none;">
<?php
include STAFFINC_DIR . 'templates/tickets.tmpl.php';
?>
</div>

<div class="tab_content" id="notes" style="display:none">
<?php
$notes = QuickNote::forOrganization($org);
$create_note_url = 'orgs/'.$org->getId().'/note';
include STAFFINC_DIR . 'templates/notes.tmpl.php';
?>
</div>

<div class="tab_content" id="visuStockTickets"  style="<?php echo $style_tp; ?>">
<?php
include STAFFINC_DIR . 'templates/visu-stock-tickets.tmpl.php';
?>
</div>
<div class="tab_content" id="reporting" style="display: none">
  <h1>Reporting de <span id="test"></span>:</h1>
  <div id="buttonYear"></div>
  <hr>
<div id="ticket" style="height: 1550px;width: auto;">
  <div style="height:250px;">
    <div style="width:50%;float:left">
      <h3 style="text-align:left;">Durée de traitement des tickets en jour pour votre entreprise:</h3>
      <a class=info href="#"><div id="GaugeMoyenne" style="float:left"></div><span>Durée moyenne des tickets jusqu'à fermeture pour votre entreprise</span></a>
      <a class=info href="#"><div id="GaugeMedianne" style="float:left"></div><span>Durée médianne des tickets jusqu'à fermeture pour votre entreprise</span></a>
    </div>
    <div style="width:50%;float:right;text-align:right">
      <h3 style="position:relative;text-align: right;margin-right:140px;">Durée de traitement des tickets en jour général:</h3>
      <a class=info href="#"><div id="GaugeMoyenneGeneral" style="float:right;position:relative;"></div><span>Durée moyenne des tickets jusqu'à fermeture pour toutes les entreprises</span></a>
      <a class=info href="#"><div id="GaugeMedianneGeneral" style="float:right;position:relative;"></div><span>Durée médianne des tickets jusqu'à fermeture pour toutes les entreprises</span></a>
    </div>
  </div>
  <div>
    <div id="histo_Reponse" style="height: 500px;width: 75%; margin-left:18%"></div>
    <div id="ferme" style="width:50%;float:left;"></div>
    <div id="ouvert" style="width:50%;float:right"></div>
  </div>
</div>
<br>
  <hr>
</div>
<script type="text/javascript">
var FormattingDatas = {
    datas : [],
    name : null,
    Init : function(_datas, _name){
        this.datas = _datas;
        this.name = _name + "";
    },
    TestDatas : function() {
        return (this.datas instanceof Array);
    },
    TestName : function() {
        return (typeof this.name == "string");
    }
};
var BasesFunctions = Object.create(FormattingDatas);
BasesFunctions.SortBySem = function(self) {
    self.datas = self.datas.sort(function (a, b) {
        return a.semaine - b.semaine;
    });
};
BasesFunctions.AllAgents = function(self){
    self.agents = self.datas.reduce(function (all, item, index) {
        if(all.indexOf(item[self.valORname]) == -1
        && item[self.valORname] != null){
        all.push(item[self.valORname]);
        }
        return all;
    }, []);
};
var Reponse = Object.create(BasesFunctions);
var TicketAgent = Object.create(BasesFunctions);
var TicketOuvertAgent = Object.create(BasesFunctions);
var TempsRep = Object.create(BasesFunctions);
var TempsReponse = Object.create(BasesFunctions);
var TempsRepMoTotal = Object.create(BasesFunctions);
var TempsRepMeTotal = Object.create(BasesFunctions);
TicketAgent.Ticket = function(_data, _year){
  this.valORname = "lastname";
  this.Init(_data, _year);
  this.SortBySem(this);
  this.AllAgents(this);

  self = this;

  var ticketBySemaine = [];
  for (semaine = 1; semaine <=52; semaine++)
  {
    var initTempObj = [];
    for(s=0; s < this.agents.length; s++)
    {
        initTempObj.push(0);
    }
    let laSemaine = "" + semaine;
    var tempObj = {};
    tempObj[laSemaine] = this.datas.reduce(function (all, item, index) {
            if (item.semaine === laSemaine) {
                all[self.agents.indexOf(item[self.valORname])] = +item.tickets;
            }
        return all;
    }, initTempObj);
    ticketBySemaine.push(tempObj);
  };
  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.agents.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Agents"]);

  ArrayToReturn.push(lineToReturn);

  ticketBySemaine.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(item);
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });

  return ArrayToReturn;
}
Reponse.ReponseCamenbert = function(_data, _name){
  this.Init(_data, _name);
  var ArrayToReturn = [];
  ArrayToReturn = [
     ["Temps","Tickets"],
     ["moins de 1 jour", 0],
     ["de 1 à 2 jours", 0],
     ["de 3 à 5 jours", 0],
     ["de 6 à 10 jours", 0],
     ["plus de 11 jours", 0]
   ];
  let monAraay = this.datas.reduce(function(all, item, index){
    if (item.deltaJours == 0) {
      all[1][1] += +item.nbTickets;
    }else if (item.deltaJours == 1 || item.deltaJours == 2){
      all[2][1] += +item.nbTickets;
    }else if (item.deltaJours >=3 && item.deltaJours <= 5){
      all[3][1] += +item.nbTickets;
    }else if (item.deltaJours >=6 && item.deltaJours <= 10){
      all[4][1] += +item.nbTickets;
    }else if (item.deltaJours >=11){
      all[5][1] += +item.nbTickets;
    }
    return all;
  },ArrayToReturn);
  return ArrayToReturn;
}
TicketOuvertAgent.Ticket = function(_data, _year){
  this.valORname = "lastname";
  this.Init(_data, _year);
  this.SortBySem(this);
  this.AllAgents(this);

  self = this;

  var ticketBySemaine = [];
  for (semaine = 1; semaine <=52; semaine++)
  {
    var initTempObj = [];
    for(s=0; s < this.agents.length; s++)
    {
        initTempObj.push(0);
    }
    let laSemaine = "" + semaine;
    var tempObj = {};
    tempObj[laSemaine] = this.datas.reduce(function (all, item, index) {
            if (item.semaine === laSemaine) {
                all[self.agents.indexOf(item[self.valORname])] = +item.tickets;
            }
        return all;
    }, initTempObj);
    ticketBySemaine.push(tempObj);
  };
  var ArrayToReturn = [];
  var lineToReturn = [];

  lineToReturn = this.agents.reduce(function (all, item, index) {
      all.push(item);
      return all;
  }, ["Agents"]);

  ArrayToReturn.push(lineToReturn);

  ticketBySemaine.forEach(function (element) {
      var tempLine = [];
      for (var key in element) {
          tempLine = element[key].reduce(function (all, item, index) {
              all.push(item);
              return all;
          }, [key]);
      };
      ArrayToReturn.push(tempLine);
  });

  return ArrayToReturn;

}
TempsRep.TR = function(_data, _name){
  this.Init(_data, _name);

  self = this;

  var ArrayToReturn = [];
  var lineToReturn = [];
  var i = 0;
  var res =0;

  this.datas.reduce(function(all, item, index){
    i++;
    res += (+item.deltaJours) * (+item.nbTickets);
  },ArrayToReturn);
  ArrayToReturn = ['Moyenne',res/i];
  return ArrayToReturn;
}
//MEDIANNE
TempsReponse.Medianne = function(_data, _name){
  this.Init(_data, _name);

  self = this;

  var ArrayToReturn = [];
  var lineToReturn = [];
  var res =0;
  var deltaJour = null;
  var medianne;

  this.datas.reduce(function(all, item, index){
    res += +item.nbTickets;
  },ArrayToReturn);

  medianne = (res+1)/2;
  if(res%2 == 0){
      this.datas.reduce(function(all,item,index){
        all += +item.nbTickets;
        if(all >= medianne && deltaJour == null){
          if (index !=0) {
            deltaJour = ((+self.datas[index-1].deltaJours) + (+self.datas[index].deltaJours))/2;
          }
        }
        return all;
      },0)
  }else{
      this.datas.reduce(function(all,item,index){
        all += +item.nbTickets;
        if(all >= medianne && deltaJour == null){
          deltaJour = +item.deltaJours;
        }
        return all;
      },0)
  }
  ArrayToReturn = ["Medianne",parseInt(deltaJour)];
  return ArrayToReturn;
}
TempsRepMoTotal.TR = function(_data){
  this.Init(_data);

  self = this;

  var ArrayToReturn = [];
  var lineToReturn = [];
  var i = 0;
  var res =0;

  this.datas.reduce(function(all, item, index){
    i++;
    res += (+item.deltaJours) * (+item.nbTickets);
  },ArrayToReturn);
  ArrayToReturn = ['Moyenne',res/i];
  return ArrayToReturn;
}

TempsRepMeTotal.TR = function(_data){
  this.Init(_data);

  self = this;

  var ArrayToReturn = [];
  var lineToReturn = [];
  var res =0;
  var deltaJour = null;
  var medianne;

  this.datas.reduce(function(all, item, index){
    res += +item.nbTickets;
  },ArrayToReturn);

  medianne = (res+1)/2;
  if(res%2 == 0){
      this.datas.reduce(function(all,item,index){
        all += +item.nbTickets;
        if(all >= medianne && deltaJour == null){
          deltaJour = ((+self.datas[index-1].deltaJours) + (+self.datas[index].deltaJours))/2;
        }
        return all;
      },0)
  }else{
      this.datas.reduce(function(all,item,index){
        all += +item.nbTickets;
        if(all >= medianne && deltaJour == null){
          deltaJour = item.deltaJours;
        }
        return all;
      },0)
  }
  ArrayToReturn = ["Medianne",parseInt(deltaJour)];
  return ArrayToReturn;
}

$(document).ready(function () {
  $(document).on('click', 'a.org-action', function(e) {
     e.preventDefault();
     var url = 'ajax.php/'+$(this).attr('href').substr(1);
     $.dialog(url, [201, 204], function (xhr) {
         if (xhr.status == 204)
             window.location.href = 'orgs.php';
         else
             window.location.href = window.location.href;
      }, {
         onshow: function() { $('#org-search').focus(); }
      });
     return false;
 });

 //si la valeur du input est négative on affiche le texte du DEBIT
 $("#creditTMA").on("change keyup", function() {
     if($(this).val() < 0){
         $("#labelCreditTMA").hide();
         $("#labelDebitTMA").show();
     }else{
         $("#labelCreditTMA").show();
         $("#labelDebitTMA").hide();
     }
 });

 //désactiver le retour chariot dans le formulaire
 $("#cout_tickets").keydown(function(event){
     if(event.keyCode == 13) {
         event.preventDefault();
         return false;
     }
 });
 var ongTicket = "<?php echo $_REQUEST["tickets"]; ?>";
 if (ongTicket == "true") {
   $('#users').attr({
     style:"display: none"
   });
   $('#users_tab').removeClass("active");
   $('#tickets').attr({
       style:"display: block"
   });
   $('#tickets_tab').addClass("active");
 }
  var monName;
  var year ="";
  <?php
   $req = "select 'name', org.name from ost_organization org where org.id =".$_REQUEST['id'];
   $res = db_query($req);
   $stats = array();
   while($row = db_fetch_row($res)) {
       $stats[$row[0]] = $row[1];
   }
   ?>
   var nameOrg = "<?php echo $stats['name']; ?>";
   monName = nameOrg;
   $('#test').html(nameOrg);
   var currentYear = new Date().getFullYear();
     $('#ticket').hide();
   for (var i = 2016; i <= currentYear; i++) {
     var random1 = Math.floor((Math.random() * 250) + 1);
     var random2 = Math.floor((Math.random() * 250) + 1);
     var random3 = Math.floor((Math.random() * 250) + 1);
      var color ="#"+(random1).toString(16)+(random2).toString(16)+(random3).toString(16);
     $('#buttonYear').append('<button type="button" id="year'+i+'" style="padding:10px 50px; background:'+color+';border-radius: 15px 15px; font-weight: bold; color: black">'+i+'</button>');
     clickGenerator(i);
     if (i == currentYear) {
       $( "#year"+i ).removeClass( "action" );
       $('#year'+i).trigger( "click" );
     }
   }
   $('#reporting_tab').click(function(){
     $('#year'+currentYear).trigger('click');
   });
   function clickGenerator(_year,flag){
     $('#year'+_year).click(function(){
       $('button[id^="year"]').addClass('action');
       $( "#year"+_year).removeClass( "action" );
       sqlDelai(_year);
       sqlDelaiAll(_year);
       sqlTicketFerme(_year);
       sqlTicketOuvert(_year);
       setTimeout(function () {
         sqlReponse(_year);
       }, 500);
       $('#ticket').show();
     });
   }
  // $('#exit').click(function(){
  //   $('#ticket').hide();
  //   $('#histo_Reponse').show();
  // });
  function sqlReponse(_year){
      var orgId = <?php echo $_REQUEST['id']; ?>;
    $.ajax({
        url: 'queryReponse.php?id='+orgId+'&year='+_year, // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: {name: monName},
        dataType: 'json', // Le type de données à recevoir
        success: function (data, statut) { // success est toujours en place, bien sûr !
           if (data) {
                datasCamenbertReponse(data, monName);
           }
        },
        error: function (resultat, statut, erreur) {
            console.log(resultat, statut, erreur);
        }
    });
  }
  function sqlTicketFerme(_year){
    $.ajax({
        url: 'queryTicketFerme.php?year='+_year+'&name='+nameOrg, // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        dataType: 'json', // Le type de données à recevoir
        success: function (data, statut) { // success est toujours en place, bien sûr !
           if (data.length>1) {
             datasToDisplayAgent(data, _year);
           }else {
             $('#ferme').html('<p style="color:red">Aucune données disponibles.</p>');
           }
        },
        error: function (resultat, statut, erreur) {
            console.log(resultat, statut, erreur);
        }
    });
  }
  function sqlTicketOuvert(_year){
    $.ajax({
        url: 'queryTicketOuvert.php?name='+nameOrg, // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: {year: +_year},
        dataType: 'json', // Le type de données à recevoir
        success: function (data, statut) { // success est toujours en place, bien sûr !
           if (data.length>1) {
                dataTicketOuvertAgent(data, _year);
           }else {
             $('#ouvert').html('<p style="color:red">Aucune données disponibles.</p>');
           }
        },
        error: function (resultat, statut, erreur) {
            console.log(resultat, statut, erreur);
        }
    });
  }
  function sqlDelai(_year){
    $.ajax({
        url: 'queryDelai.php?year='+_year, // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: {name: nameOrg},
        dataType: 'json', // Le type de données à recevoir
        success: function (data, statut) { // success est toujours en place, bien sûr !
          if (data.length > 0) {
            dataGaugeDelai(data, nameOrg);
            dataGaugeMedianne(data, nameOrg);
          }else {
            $('#GaugeMedianne').html('pas de valeur');
          }
        },
        error: function (resultat, statut, erreur) {
            console.log(resultat, statut, erreur);
        }
    });
  }
  function sqlDelaiAll(_year){
    $.ajax({
        url: 'queryDelaiAll.php?year='+_year, // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        dataType: 'json', // Le type de données à recevoir
        success: function (data, statut) { // success est toujours en place, bien sûr !
           if (data.length > 1) {
             dataGaugeMoyenneTotal(data);
             dataGaugeMedianneTotal(data);
           }else {
             $('#delai').html('<h3 style="color:red">Aucun ticket pour cette société.</h3>');
             $('#delai').show();
           }
        },
        error: function (resultat, statut, erreur) {
            console.log(resultat, statut, erreur);
        }
    });
  }
  var datasCamenbertReponse = function (_data, _name) {
      var myDatas = Reponse.ReponseCamenbert(_data, _name);
      drawChartReponseCamenbert(myDatas);
  };
  var datasToDisplayAgent = function (_data, _year) {
    var myDatas = TicketAgent.Ticket(_data, _year);
    drawChartAgent(myDatas);
  };
  var dataTicketOuvertAgent = function (_data, _year) {
    var myDatas = TicketOuvertAgent.Ticket(_data, _year);
    drawChartTicketOuvertAgent(myDatas);
  };

  var dataGaugeDelai = function (_data, _name) {
    var myDatas = TempsRep.TR(_data, _name);
    drawGaugeDelai(myDatas);
  }

  var dataGaugeMedianne = function (_data,_name){
    var myDatas = TempsReponse.Medianne(_data,_name);
    drawGaugeMedianne(myDatas);
  }
  var dataGaugeMoyenneTotal = function (_data) {
    var myDatas = TempsRepMoTotal.TR(_data);
    drawGaugeMoyenneTotal(myDatas);
  }

  var dataGaugeMedianneTotal = function (_data) {
    var myDatas = TempsRepMeTotal.TR(_data);
    drawGaugeMedianneTotal(myDatas);
  }

  function drawChartReponseCamenbert(datas){
    var data = google.visualization.arrayToDataTable(datas);
    var options = {
            title: 'Durée de traitement jusqu\'a fermeture des tickets',
            is3D: true,
          };

          var chart = new google.visualization.PieChart(document.getElementById("histo_Reponse"));
          chart.draw(data, options);
  };
  function drawChartAgent(datas) {
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "Tickets fermés par semaine (agent)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'Tickets fermés'
        },
        vAxis:{
          title: 'semaines'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("ferme"));
    chart.draw(data, options);
  }
};
function drawChartTicketOuvertAgent(datas){
  if (datas instanceof Array) {
    var data = google.visualization.arrayToDataTable(datas);

    var options = {
        title: "Tickets ouverts par semaine (agent)",
        height:800,
        egend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true,
        hAxis:{
          title: 'Tickets ouverts'
        },
        vAxis:{
          title: 'semaines'
        }
    };
    var chart = new google.visualization.BarChart(document.getElementById("ouvert"));
    chart.draw(data, options);
  }
}
function drawGaugeDelai(datas){
  var data = google.visualization.arrayToDataTable([['Label', 'Value'],datas]);

 var options = {
   width: 600, height: 200,
   redFrom: 90, redTo: 100,
   yellowFrom:75, yellowTo: 90,
   greenFrom:0, greenTo: 25,
   minorTicks: 5
 };

 var gauge = new google.visualization.Gauge(document.getElementById('GaugeMoyenne'));

 gauge.draw(data, options);
}

function drawGaugeMedianne(datas){
  var data = google.visualization.arrayToDataTable([['Label', 'Value'],datas]);

 var options = {
   width: 600, height: 200,
   redFrom: 90, redTo: 100,
   yellowFrom:75, yellowTo: 90,
   greenFrom:0, greenTo: 25,
   minorTicks: 5
 };

 var gauge = new google.visualization.Gauge(document.getElementById('GaugeMedianne'));

 gauge.draw(data, options);

}
function drawGaugeMoyenneTotal(datas){
  var data = google.visualization.arrayToDataTable([['Label', 'Value'],datas]);

  var options = {
    width: 600, height: 200,
    redFrom: 180, redTo: 200,
    yellowFrom:150, yellowTo: 180,
    greenFrom:0, greenTo: 50,
    minorTicks: 5, max: 200
  };

  var gauge = new google.visualization.Gauge(document.getElementById('GaugeMoyenneGeneral'));

  gauge.draw(data, options);
}

function drawGaugeMedianneTotal(datas){
  var data = google.visualization.arrayToDataTable([['Label', 'Value'],datas]);

  var options = {
    width: 600, height: 200,
    redFrom: 90, redTo: 100,
    yellowFrom:75, yellowTo: 90,
    greenFrom:0, greenTo: 25,
    minorTicks: 5
  };

  var gauge = new google.visualization.Gauge(document.getElementById('GaugeMedianneGeneral'));

  gauge.draw(data, options);
}
});
</script>
