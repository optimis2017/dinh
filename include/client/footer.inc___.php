        </div>
    </div>
    
    <?php
if($_REQUEST["caroussel"] == 2){
?>
<div id="container_caroussel">
    <div class="myCaroussel_footer">
        <div class="wagonnet"><img src="<?php echo ROOT_PATH.'images/caroussel/oemail.png'; ?>"/></div>
        <div class="wagonnet"><img src="<?php echo ROOT_PATH.'images/caroussel/opicking.png'; ?>"/></div>
        <div class="wagonnet"><img src="<?php echo ROOT_PATH.'images/caroussel/otrm.png'; ?>"/></div>
    </div>
</div>
<?php
} 
?>
    
    
    <div id="footer">
        <p>Copyright &copy; <?php echo date('Y'); ?> <?php echo (string) $ost->company ?: 'osTicket.com'; ?> - All rights reserved.</p>
        <a id="poweredBy" href="http://osticket.com" target="_blank"><?php echo __('Helpdesk software - powered by osTicket'); ?></a>
    </div>
<div id="overlay"></div>
<div id="loading">
    <h4><?php echo __('Please Wait!');?></h4>
    <p><?php echo __('Please wait... it will take a second!');?></p>
</div>
<?php
if (($lang = Internationalization::getCurrentLanguage()) && $lang != 'en_US') { ?>
    <script type="text/javascript" src="ajax.php/i18n/<?php
        echo $lang; ?>/js"></script>
<?php } ?>
        
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
     
 <script type="text/javascript">
    $(document).ready(function(){
      $('.your-class').slick({
          dots: true,
          autoplay: true,
          infinite: true
      });
      
      $('.myCaroussel_footer').slick({
          dots: true,
          autoplay: true,
          infinite: true,
          slidesToShow: 1,
          centerMode: true,
          variableWidth: true
      });
    });
</script>

</body>
</html>
