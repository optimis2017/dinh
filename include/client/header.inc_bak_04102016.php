<?php
include_once(INCLUDE_DIR.'class.ticket_payant.php');

$title=($cfg && is_object($cfg) && $cfg->getTitle())
    ? $cfg->getTitle() : 'osTicket :: '.__('Support Ticket System');
$signin_url = ROOT_PATH . "login.php"
    . ($thisclient ? "?e=".urlencode($thisclient->getEmail()) : "");
$signout_url = ROOT_PATH . "logout.php?auth=".$ost->getLinkToken();

header("Content-Type: text/html; charset=UTF-8\r\n");
?>
<!DOCTYPE html>
<html <?php
if (($lang = Internationalization::getCurrentLanguage())
        && ($info = Internationalization::getLanguageInfo($lang))
        && (@$info['direction'] == 'rtl'))
    echo 'dir="rtl" class="rtl"';


$contrat = 'optimis'; // valeur par defaut du contrat de maintenance (ça va servir à afficher soit optimis soit avea
if( isset($thisclient)){
    $query = 'SELECT value FROM '.FORM_ANSWER_TABLE.'
    where field_id = 34
    and entry_id =  (SELECT entry.id FROM '.FORM_ENTRY_TABLE.' as entry
    where entry.form_id = ( SELECT id FROM '.FORM_SEC_TABLE.' where  type = \'O\')
    and entry.object_type = \'O\'
    and entry.object_id = (select ost_user.org_id from '.USER_TABLE.' 
    where id = (SELECT user_id FROM '.USER_ACCOUNT_TABLE.'
    where id = '.$thisclient->getAccount()->ht['id'].')))';
    
        //print_r($query);
        // Fetch the results
        $results = array();
        $res = db_query($query);
        while ($row = db_fetch_array($res))
            $results = split('"',$row['value']);
        
        if($results[3] == 'Contrat Avea'){
            $contrat = 'avea';
        }


        if ($contrat == 'avea') {
            $_SESSION["design"] = "avea";
            $GLOBALS['indexLink'] = "index-avea.php";
            $GLOBALS['logo'] = "logoavea.png";
        }
         if ($contrat == 'optimis') {
            $_SESSION["design"] = "";
            $GLOBALS['indexLink'] = "index.php";
            $GLOBALS['logo'] = "logo.png";
        }
    
    }
    
    if(!isset($GLOBALS['logo'])){
        switch ($_SESSION['design']) {
            case 'avea':
                $GLOBALS['indexLink'] = "index-avea.php";
                $GLOBALS['logo'] = "logoavea.png";
                break;

            default:
                $GLOBALS['indexLink'] = "index.php";
                $GLOBALS['logo'] = "logo.png";
                break;
        }       
    }    


if ($_SESSION["design"] == "avea") {
    $faviconLink = "images/favicon_Avea.png";
}else{
    $faviconLink = "images/favicon.ico";
}
?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo Format::htmlchars($title); ?></title>
    <meta name="description" content="customer support platform">
    <meta name="keywords" content="osTicket, Customer support system, support ticket system">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="<?php echo ROOT_PATH.$faviconLink; ?>">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/osticket.css?1faad22" media="screen">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/theme.css?1faad22" media="screen">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/print.css?1faad22" media="print">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>scp/css/typeahead.css"
         media="screen" />
    <link type="text/css" href="<?php echo ROOT_PATH; ?>css/ui-lightness/jquery-ui-1.10.3.custom.min.css"
        rel="stylesheet" media="screen" />
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/thread.css?1faad22" media="screen">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/redactor.css?1faad22" media="screen">
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/font-awesome.min.css?1faad22">
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/flags.css?1faad22">
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/rtl.css?1faad22"/>
    
    
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/slick_159/slick.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/slick_159/slick-theme.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/caroussel.css"/>
    
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-1.8.3.min.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui-1.10.3.custom.min.js?1faad22"></script>
    <script src="<?php echo ROOT_PATH; ?>js/osticket.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/filedrop.field.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery.multiselect.min.js?1faad22"></script>
    <script src="<?php echo ROOT_PATH; ?>scp/js/bootstrap-typeahead.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor.min.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor-osticket.js?1faad22"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor-fonts.js?1faad22"></script>
    <?php
    if($ost && ($headers=$ost->getExtraHeaders())) {
        echo "\n\t".implode("\n\t", $headers)."\n";
    }
    ?>
</head>
<body>
    <?php
////    $query = 'SELECT user_id FROM ' . USER_ACCOUNT_TABLE . ' where id = ' . $thisclient->getAccount()->ht['id'];
//$contrat = 'optimis'; // valeur par defaut du contrat de maintenance (ça va servir à afficher soit optimis soit avea
//if( isset($thisclient)){
//    $query = 'SELECT value FROM '.FORM_ANSWER_TABLE.'
//    where field_id = 34
//    and entry_id =  (SELECT entry.id FROM '.FORM_ENTRY_TABLE.' as entry
//    where entry.form_id = ( SELECT id FROM '.FORM_SEC_TABLE.' where  type = \'O\')
//    and entry.object_type = \'O\'
//    and entry.object_id = (select ost_user.org_id from '.USER_TABLE.' 
//    where id = (SELECT user_id FROM '.USER_ACCOUNT_TABLE.'
//    where id = '.$thisclient->getAccount()->ht['id'].')))';
//    
//        //print_r($query);
//        // Fetch the results
//        $results = array();
//        $res = db_query($query);
//        while ($row = db_fetch_array($res))
//            $results = split('"',$row['value']);
//        
//        if($results[3] == 'Contrat Avea'){
//            $contrat = 'avea';
//        }
//
//
//        if ($contrat == 'avea') {
//            $_SESSION["design"] = "avea";
//            $GLOBALS['indexLink'] = "index-avea.php";
//            $GLOBALS['logo'] = "logoavea.png";
//        }
//         if ($contrat == 'optimis') {
//            $_SESSION["design"] = "";
//            $GLOBALS['indexLink'] = "index.php";
//            $GLOBALS['logo'] = "logo.png";
//        }
//    
//    }
//    
//    if(!isset($GLOBALS['logo'])){
//        switch ($_SESSION['design']) {
//            case 'avea':
//                $GLOBALS['indexLink'] = "index-avea.php";
//                $GLOBALS['logo'] = "logoavea.png";
//                break;
//
//            default:
//                $GLOBALS['indexLink'] = "index.php";
//                $GLOBALS['logo'] = "logo.png";
//                break;
//        }       
//    }    
    
//    echo '<pre>';
//    print_r("<br>-----------header.inc.php--------------".$GLOBALS['logo']);
//    print_r('<br>-----------header.inc.php--------------'.$GLOBALS['indexLink']);
//    print_r('<br>-----------header.inc.php--------------'.$_SESSION['design']);
//    echo '</pre>';
    
    ?>
    <div id="container">
        <div id="header">
<!--            <a class="pull-left" id="logo" href="<?php echo ROOT_PATH; ?>index.php"-->
            <a class="pull-left" id="logo" href="<?php echo ROOT_PATH.$GLOBALS['indexLink']; ?>"

            title="<?php echo __('Support Center'); ?>"><img src="<?php
                echo ROOT_PATH."logo.php?logo=".$GLOBALS['logo']; ?>" border=0 alt="<?php
                echo $ost->getConfig()->getTitle(); ?>"
                style="height: 5em"></a>
            <div class="pull-right flush-right">
            <p>
             <?php
                if ($thisclient && is_object($thisclient) && $thisclient->isValid()
                    && !$thisclient->isGuest()) {
                    
                    $solde_tma = new Ticket_payant();
                    $solde_tma->get_TMAtot($thisclient->getId());
                    
                 echo Format::htmlchars($thisclient->getName()).'&nbsp;|';
                 echo __(' solde T.M.A. ('.$solde_tma->quantite).')&nbsp;|';
                 ?>
                <a href="<?php echo ROOT_PATH; ?>profile.php"><?php echo __('Profile'); ?></a> |
                <a href="<?php echo ROOT_PATH; ?>tickets.php"><?php echo sprintf(__('Tickets <b>(%d)</b>'), $thisclient->getNumTickets()); ?></a> -
                <a href="<?php echo $signout_url; ?>"><?php echo __('Sign Out'); ?></a>
            <?php
            } elseif($nav) {
                if ($cfg->getClientRegistrationMode() == 'public') { ?>
                    <?php echo __('Guest User'); ?> | <?php
                }
                if ($thisclient && $thisclient->isValid() && $thisclient->isGuest()) { ?>
                    <a href="<?php echo $signout_url; ?>"><?php echo __('Sign Out'); ?></a><?php
                }
                elseif ($cfg->getClientRegistrationMode() != 'disabled') { ?>
                    <a href="<?php echo $signin_url; ?>"><?php echo __('Sign In'); ?></a>
<?php
                }
            } ?>
            </p>
            <p>
<?php
if (($all_langs = Internationalization::availableLanguages())
    && (count($all_langs) > 1)
) {
    foreach ($all_langs as $code=>$info) {
        list($lang, $locale) = explode('_', $code);
?>
        <a class="flag flag-<?php echo strtolower($locale ?: $info['flag'] ?: $lang); ?>"
            href="?<?php echo urlencode($_GET['QUERY_STRING']); ?>&amp;lang=<?php echo $code;
            ?>" title="<?php echo Internationalization::getLanguageDescription($code); ?>">&nbsp;</a>
<?php }
} ?>
            </p>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        if($nav){ ?>
        <ul id="nav" class="flush-left">
            <?php
            if($nav && ($navs=$nav->getNavLinks()) && is_array($navs)){
                foreach($navs as $name =>$nav) {
                    if ($name == 'home') {
                        $nav['href'] = $GLOBALS['indexLink'];
                    }
                    echo sprintf('<li><a class="%s %s" href="%s">%s</a></li>%s',$nav['active']?'active':'',$name,(ROOT_PATH.$nav['href']),$nav['desc'],"\n");
                }
            } ?>
        </ul>
        <?php
        }else{ ?>
         <hr>
        <?php
        } ?>
        <div id="content">

         <?php if($errors['err']) { ?>
            <div id="msg_error"><?php echo $errors['err']; ?></div>
         <?php }elseif($msg) { ?>
            <div id="msg_notice"><?php echo $msg; ?></div>
         <?php }elseif($warn) { ?>
            <div id="msg_warning"><?php echo $warn; ?></div>
         <?php } ?>
