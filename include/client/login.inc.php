<?php
if(!defined('OSTCLIENTINC')) die('Access Denied');

$email=Format::input($_POST['luser']?:$_GET['e']);
$passwd=Format::input($_POST['lpasswd']?:$_GET['t']);

$content = Page::lookup(Page::getIdByType('banner-client'));

if ($content) {
    list($title, $body) = $ost->replaceTemplateVariables(
        array($content->getName(), $content->getBody()));
} else {
    $title = __('Sign In');
    $body = __('To better serve you, we encourage our clients to register for an account and verify the email address we have on record.');
}

function allready_mail($theMailAdress){
    $query ='SELECT id FROM '.USER_EMAIL_TABLE.' WHERE address=\''.$theMailAdress.'\'';

    //print_r($query);
    // Fetch the results
    $results = array();
    $res = db_query($query);
    while ($row = db_fetch_array($res))
        $results[$row['user_id']] = $row;

    if ($results) {
        return TRUE;
    }else{
        return FALSE;
    }
}

?>
<!--<h1><?php echo Format::display($title); ?></h1>-->
<h1><?php echo Format::display("Accès au support"); ?></h1>
<p><?php echo Format::display($body); ?></p>
<form action="login.php" method="post" id="clientLogin" class="form1_new_user_<?php echo $_REQUEST['new_user']; ?>">
    <?php csrf_token(); ?>
<div style="display:table-row">
    <div class="login-box">
    <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
    <div>
        <input id="username" placeholder="<?php echo __('Email or Username'); ?>" type="text" name="luser" size="30" value="<?php echo $email; ?>">
    </div>
    <div>
        <input id="passwd" placeholder="<?php echo __('Password'); ?>" type="password" name="lpasswd" size="30" value="<?php echo $passwd; ?>"></td>
    </div>
    <p>
        <input class="btn" type="submit" value="<?php echo __('Sign In'); ?>">
<?php if ($suggest_pwreset) { ?>
        <a style="padding-top:4px;display:inline-block;" href="pwreset.php"><?php echo __('Forgot My Password'); ?></a>
<?php } ?>
    </p>
    </div>
    <div style="display:table-cell;padding: 15px;vertical-align:top">
<?php

$ext_bks = array();
foreach (UserAuthenticationBackend::allRegistered() as $bk)
    if ($bk instanceof ExternalAuthentication)
        $ext_bks[] = $bk;

if (count($ext_bks)) {
    foreach ($ext_bks as $bk) { ?>
<div class="external-auth"><?php $bk->renderExternalLink(); ?></div><?php
    }
}
if ($cfg && $cfg->isClientRegistrationEnabled()) {
    if (count($ext_bks)) echo '<hr style="width:70%"/>'; ?>
    <div style="margin-bottom: 5px">
    <?php echo __('Not yet registered?'); ?> <a href="account.php?do=create"><?php echo __('Create an account'); ?></a>
    </div>
<?php } ?>
    <div>
    <b><?php echo __("I'm an agent"); ?></b> —
    <a href="<?php echo ROOT_PATH; ?>scp"><?php echo __('sign in here'); ?></a>
    </div>
    <div>
        <br>
    <b><?php echo __("Forgot My Password"); ?></b> —
    <a style="padding-top:4px;display:inline-block;" href="pwreset.php"><?php echo __('Demander un nouveau mot de passe'); ?></a>
    </div>
    <div>
        <br>
    <b><?php echo __("Je suis un nouvel utilisateur"); ?></b> —
    <a href="<?php echo ROOT_PATH; ?>login.php?new_user=ok"><?php echo __('Demander l\'ouverture d\'un compte'); ?></a>
    </div>
    </div>
</div>
</form>

<form action="login.php?mail_send=ok&new_user=ok" method="post" id="clientLogin" class="form2_new_user_<?php echo $_REQUEST['new_user']; ?>" >
    <?php 
        csrf_token();
        $valEmail = "";
        $valName = "";
        $valTel = "";
        $valFirm = "";
        if($_REQUEST['mail_send'] == 'ok'){
            $valEmail = trim($_POST['newEmail']);
            
            if(allready_mail($valEmail)){
                $messageFinNewUser = "Cette addresse mail existe déjà!";
                $majMdp = "ok";
            }else{                
                $valName = $_POST['newName'];
                $valSurName = $_POST['newSurName'];
                $valTel = $_POST['newTel'];
                $valFirm = $_POST['newFirm']; 
                $client = "Optimis";
                if($_SESSION["design"] == "avea"){
                    $client = "Avea";
                }

                $newTitleAdmin = 'Demande inscription support -- %1$s -- %2$s';
                $newTitleAdmin = sprintf($newTitleAdmin, $valName, $valFirm);
                $newMessageAdmin = 'demande d\'inscription au support
                
email: %1$s
nom: %2$s
prenom: %5$s
tel: %3$s
société: %4$s
client: %6$s
';
            $newMessageClient = 'Bonjour %5$s %2$s,

​Vous avez fait une demande de création de compte sur le portail d\'assistance. Nous vous en remercions.
Après contrôle, un mail de création de compte va vous être envoyé.
Vous devrez alors créer votre mot de passe.
 ​
​R​écapitulatif​ de votre demande​:

email:   %1$s
nom:     %2$s
prenom:  %5$s
tel:     %3$s
société: %4$s

Cordialement.
Support %6$s
04 28 29 07 48
';
        
                $newMessageAdmin = sprintf($newMessageAdmin, $valEmail, $valName, $valTel, $valFirm, $valSurName, $client);
                $newMessageClient = sprintf($newMessageClient, $valEmail, $valName, $valTel, $valFirm, $valSurName, $client);
                $objetDuMail = sprintf('support client %1$s | demande d\'inscription', $client);
                $email = $cfg->getDefaultEmail();

                $email->send($valEmail,
                Format::striptags($objetDuMail), nl2br($newMessageClient));
                $email->send($cfg->config['admin_email']['value'],
                Format::striptags($newTitleAdmin), nl2br($newMessageAdmin));

                $messageFinNewUser = "Merci, vous allez recevoir un mail récapitulant votre demande";
            }
        }
//        echo '<pre>';
//        print_r($newMessageClient);
//        print_r($newMessageAdmin);
//        echo '</pre>';
    ?>
<div style="display:table-row">
    <div class="login-box">
    <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
    <div>
        <input id="email" class="form2_new_user" placeholder="<?php echo __('Email'); ?>" required="" 
               type="email" name="newEmail" size="30" value="<?php echo $valEmail; ?>">
    </div>
    <div>
        <input id="name" class="form2_new_user" placeholder="<?php echo __('Nom'); ?>" required="" 
               pattern="[a-zA-Z\- ]+" type="text" name="newName" size="30" value="<?php echo $valName; ?>"></td>
    </div>
    <div>
        <input id="surname" class="form2_new_user" placeholder="<?php echo __('Prenom'); ?>" required="" 
               pattern="[a-zA-Z\- ]+" type="text" name="newSurName" size="30" value="<?php echo $valSurName; ?>"></td>
    </div>
    <div>
        <input id="tel" class="form2_new_user" placeholder="<?php echo __('Tel'); ?>" required="" 
               pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" 
               type="text" name="newTel" size="30" value="<?php echo $valTel; ?>"></td>
    </div>
    <div>
        <input id="societe" class="form2_new_user" placeholder="<?php echo __('Firm'); ?>" required="" 
               type="text" name="newFirm" size="30" value="<?php echo $valFirm; ?>"></td>
    </div>
    <p>
        <input class="btn" type="submit" value="<?php echo __('Demander'); ?>">
<?php if ($suggest_pwreset) { ?>
        <a style="padding-top:4px;display:inline-block;" href="pwreset.php"><?php echo __('Forgot My Password'); ?></a>
<?php } ?>
    </p>
    </div>
    <div style="display:table-cell;padding: 15px;vertical-align:top">
<?php

$ext_bks = array();
foreach (UserAuthenticationBackend::allRegistered() as $bk)
    if ($bk instanceof ExternalAuthentication)
        $ext_bks[] = $bk;

if (count($ext_bks)) {
    foreach ($ext_bks as $bk) { ?>
<div class="external-auth"><?php $bk->renderExternalLink(); ?></div><?php
    }
}
if ($cfg && $cfg->isClientRegistrationEnabled()) {
    if (count($ext_bks)) echo '<hr style="width:70%"/>'; ?>
    <div style="margin-bottom: 5px">
    <?php echo __('Not yet registered?'); ?> <a href="account.php?do=create"><?php echo __('Create an account'); ?></a>
    </div>
<?php } ?>
    <div>
    <b><?php echo __("I'm an agent"); ?></b> —
    <a href="<?php echo ROOT_PATH; ?>scp"><?php echo __('sign in here'); ?></a>
    </div>
    <div>
        <br>
    <b><?php echo __("Je suis un utilisateur enregistré"); ?></b> —
    <a href="<?php echo ROOT_PATH; ?>login.php"><?php echo __('Connectez-vous'); ?></a>
    </div>
    <div>
        <br><br>
        <b id="finNewUser"><?php echo $messageFinNewUser; ?></b><?php if($majMdp == 'ok'){ echo ' —'; } ?>
        <a href="pwreset.php" class="maj_mdp_<?php echo $majMdp; ?>"><?php echo __('Demander nouveau mot de passe'); ?></a>
    </div>
    </div>
</div>
</form>


<br>
<p>
<?php if ($cfg && !$cfg->isClientLoginRequired()) {
    echo sprintf(__('If this is your first time contacting us or you\'ve lost the ticket number, please %s open a new ticket %s'),
        '<a href="open.php">', '</a>');
} ?>
</p>
