<?php
/**
 * Description of class
 *
 * @author dev
 */
include_once(INCLUDE_DIR.'mysli.php');

class Ticket_payant {

    public $org_id;
    public $created;
    public $quantite;
    public $commentaire;
    public $ticket_id;
    public $tmaByTicketId;
    public $queryString;

    function __construct($oId="", $qt="", $com="", $tid="", $crted="") {
        $this->org_id = $oId;
        $this->quantite = $qt;
        $this->commentaire = $com;
        $this->ticket_id = $tid;
        $this->created = $crted;
    }

    function new_cout(){
        $sql ='INSERT INTO '.STOCK_TICKET_TABLE.' SET '
            .' `org_id`='.db_input($this->org_id)
            .' ,`created`=NOW()'
            .' ,`quantite`='.db_input($this->quantite)
            .' ,`ticket_id`='.db_input($this->ticket_id)
            .' ,`commentaire`='.db_input($this->commentaire);

//        print_r($sql);

        if(!db_query($sql))
            return FALSE;

        return TRUE;
    }

    function get_QtTMA_by_ticketId($orgId=""){
        if(!empty($this->org_id)){
            $orgId = $this->org_id;
        }

        if(!empty($orgId)){
            //Vider $this->tmaByTicketId
            $this->tmaByTicketId = array();
            $select =' SELECT SUM(stock.quantite) AS qtTMA, stock.ticket_id  ';
            $from =' FROM '.STOCK_TICKET_TABLE.' stock  ';
            $where =' WHERE stock.org_id = '.$orgId;

            $query ="$select $from $where  GROUP BY stock.ticket_id ";

            // Fetch the results
            $results = array();
            $res = db_query($query);
            if($res && $num=db_num_rows($res)){
                while ($row = db_fetch_array($res)){
                    $this->tmaByTicketId[$row['ticket_id']] = $row['qtTMA'];
                }
            }
        }

    }


    function get_TMAtot($userId){
        $select =' SELECT sum(stock.quantite) AS qtTMA, stock.org_id ';
        $from =' FROM '.STOCK_TICKET_TABLE.' stock ';
        $where =' WHERE stock.org_id = (SELECT org_id FROM '.USER_TABLE.' WHERE id='.$userId.') ';

        $query ="$select $from $where ";

        $res = db_query($query);
        if($res && $num=db_num_rows($res)){
            while ($row = db_fetch_array($res)){
                $this->org_id = $row['org_id'];
                $this->quantite = $row['qtTMA'];
            }
        }
    }
    function updateTMA($ticket_id){
      $this->queryString ="UPDATE ost_stock_tickets stock set stock.quantite = ".$this->quantite.", stock.commentaire = \"".$this->commentaire."\"
      WHERE stock.id = ".$ticket_id;
      $res = db_query($this->queryString);
      return $res;
    }

}
