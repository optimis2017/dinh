<?php
/*********************************************************************
    module.search.php

    newSearch Engine for osTicket

    A reference search engine backend is provided which uses MySQL MyISAM
    tables. This default backend should not be used on Galera clusters.

    Fred Audy <fredaudy@gmail.com>

**********************************************************************/


/**
 * Description of class
 *
 * @author dev
 */
class mySearch {
    //put your code here
    public $arrayToSearch = [];    
    
    function __construct($stringToSearch){
        $this->arrayToSearch = explode(" ", $stringToSearch);        
        $tableauKeysToSplice = [];
        
        // recup des $key où la valeur de arrayToSearch est empty
        foreach ($this->arrayToSearch as $key => $value) {
            $this->arrayToSearch[$key] = trim($value);
            
            if (empty($this->arrayToSearch[$key])){
                array_push($tableauKeysToSplice, $key);                
            }
        }        
        
        // on supprime tous les éléments empty du array arrayToSearch
        for ($i = (count($tableauKeysToSplice) - 1); $i >= 0; $i--){
            array_splice($this->arrayToSearch, $tableauKeysToSplice[$i], 1);
        } 
    }
    
    
    /*
     * construction d'une requête de la forme :
     * 
     * SELECT T.ticket_id, 
       (MATCH (T.body) AGAINST ('ligne' IN BOOLEAN MODE) +  
        MATCH (T.body) AGAINST ('consommation' IN BOOLEAN MODE)) AS cpt  
        FROM ost_ticket_thread AS T  
        WHERE  MATCH (T.body) AGAINST ('ligne' IN BOOLEAN MODE) 
        AND  MATCH (T.body) AGAINST ('consommation' IN BOOLEAN MODE)    
        ORDER BY cpt DESC
     * 
     */
    function getSearchQuery($table, $champs){
        $select = 'SELECT T.ticket_id';
        $from = sprintf(' FROM %1s AS T ', $table);
        $where = ' WHERE ';
        $order = ' ORDER BY cpt DESC';
        
        foreach ($this->arrayToSearch as $key=>$value) {
            $prefixe = ' + ';
            $and = 'AND';
            if ($key == 0) {
                $prefixe = ', (';
            }
            if ($key == (count($this->arrayToSearch) - 1)) {
                 $and = '';
            }

            $newSelect = ' %1$s MATCH (T.%3$s) AGAINST (\'%2$s\' IN BOOLEAN MODE) ';
            $newSelect = sprintf($newSelect, $prefixe, $value, $champs);
            
            $select .= $newSelect;
            
            $newWhere  = ' MATCH (T.%3$s) AGAINST (\'%2$s\' IN BOOLEAN MODE) %1$s ';
            $newWhere = sprintf($newWhere, $and, $value, $champs);
            
            $where .= $newWhere;
        }
        
        $select .= ') AS cpt ';
        
        return $select.$from.$where.$order;
    }
    
}