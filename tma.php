<?php
/*********************************************************************
    tma.php

    Main client/user interface.
    Note that we are using external ID. The real (local) ids are hidden from user.

    stagiaire Dinh

**********************************************************************/
require('client.inc.php');
$nav = new UserNav($thisclient, 'tma');
$inc ='templates/visu-stock-tickets.tmpl.php';
include(CLIENTINC_DIR.'header.inc.php');
?>
<div class="tab_content" id="visuStockTickets" style="display:block">
  <?php

  $select ='SELECT stock.id, stock.org_id, stock.created, stock.quantite, stock.ticket_id, stock.commentaire, '
          .' (SELECT tickets.number FROM '.TICKET_TABLE.' as tickets WHERE tickets.ticket_id = stock.ticket_id) AS numTicket ';

  $from =' FROM '.STOCK_TICKET_TABLE.' stock '
        .' LEFT JOIN '.ORGANIZATION_TABLE.' organization ON organization.id = stock.org_id '
        .'LEFT JOIN '.USER_TABLE.' user ON user.org_id = organization.id';

  $where = 'WHERE stock.org_id = organization.id and  user.id = '.db_input($thisclient->getId());
  TicketForm::ensureDynamicDataView();

  //$qs = array();
  $total=db_count('SELECT count(DISTINCT stock.id) '.$from.' '.$where);
  $page=($_GET['p'] && is_numeric($_GET['p']))?$_GET['p']:1;
  $pageNav=new Pagenate($total,$page,PAGE_LIMIT);
  //$pageNav=new Pagenate($total,$page,3);
  //$qstr = '&amp;'. Http::build_query($qs);
  //$qs += array('sort' => $_REQUEST['sort'], 'order' => $_REQUEST['order']);
  $pageNav->setURL('tma.php', array('id' => db_input($thisclient->getId()),'tp' => 1 ));
  //$qstr.='&amp;order='.($order=='DESC' ? 'ASC' : 'DESC');


  $query ="$select $from $where ORDER BY stock.created DESC LIMIT ".$pageNav->getStart().",".$pageNav->getLimit();
  //print_r($query);

  // Fetch the results
  $results = array();
  $res = db_query($query);
  if($res && $num=db_num_rows($res)){
      while ($row = db_fetch_array($res)){
          $results[$row['id']] = $row;
      }
  }

  $qhash = md5($query);
  $_SESSION['orgs_qs_'.$qhash] = $query;
  $_SESSION['come_from'] = 'visu_tma';

  $solde_stock_tickets=db_count('SELECT sum(quantite) '.$from.' '.$where);
  //print_r($solde_stock_tickets);
  //print_r('SELECT sum(quantite) '.$from.' '.$where);
  ?>
  <div class="pull-left titre_table_tickets">
     <?php
  //    if ($results) {
  //        echo '<strong>'.sprintf(_N('Showing %d ticket', 'Showing %d tickets',
  //            count($results)), count($results)).'</strong>';
  //    } else {
  //        echo sprintf(__('%s does not have any tickets'), $user? 'User' : 'Organization');
  //    }
      echo '<span id="text_solde_tickets">&emsp;Historique des commandes/utilisations de T.M.A. de votre entreprise&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span>';
      echo '<span id="solde_tickets">Solde : '.$solde_stock_tickets.'</span>';
  //text-align: left;
  //padding: 5px;
  //background: #929292;
  //color: #fff;
  //font-weight: bold;
     ?>
  </div>
  <div class="pull-right flush-right" style="padding-right:5px;">
      <?php //-----------------------------------------------------------------------PAS COMPRIS L'UTILITEE DE CE PASSAGE
  //    if ($user) { ?>
  <!--    <b><a class="Icon newTicket" href="tickets.php?a=open&uid=<?php // echo $user->getId(); ?>">
      <?php // print __('Create New Ticket'); ?></a></b>-->
      <?php
  //    } ?>
  </div>
  <br/>
  <div>
  <?php
  if ($results) { ?>
  <?php csrf_token(); ?>
  <!-- <input type="hidden" name="a" value="mass_process" >
   <input type="hidden" name="do" id="action" value="" >-->
   <table class="list table_tickets" border="0" cellspacing="1" cellpadding="2" width="940">
      <thead>
          <tr>
              <?php
              if (0) {?>
              <th width="8px">&nbsp;</th>
              <?php
              } ?>
  <!--            <th width="70"><?php // echo __('Ticket'); ?></th>-->
              <th class="table_tickets_date"><?php echo __('Date'); ?></th>
              <th class="table_tickets_qtachat"><?php echo __('Quantité achetée'); ?></th>
              <th class="table_tickets_qtconso"><?php echo __('Quantité consommée'); ?></th>
              <th class="table_tickets_tref"><?php echo __('ticket ref'); ?></th>
              <th class="table_tickets_com"><?php echo __('Commentaires'); ?></th>
          </tr>
      </thead>
      <tbody>
      <?php
      foreach($results as $row) {
          $row['qt_achetee'] = '-';
          $row['qt_consommee'] = '-';
          if ($row['quantite'] >= 0){
              $row['qt_achetee'] = $row['quantite'];
          }else{
               $row['qt_consommee'] = $row['quantite'];
               $row['url_ticket'] = 'tickets.php?'.Http::build_query(array('id' => $row['ticket_id']));
          }
  //        echo '<pre>';
  //        print_r($row);
  //        echo '</pre>';
          ?>
          <tr id="<?php echo $row['ticket_id']; ?>">
              <td align="center" nowrap><?php echo Format::db_datetime($row['created']); ?></td>
              <td align="center" nowrap><?php echo $row['qt_achetee']; ?></td>
              <td align="center" nowrap><?php echo $row['qt_consommee']; ?></td>
              <td align="center" nowrap>
                  <a href="<?php echo $row['url_ticket']; ?>">
                      <?php echo $row['numTicket']; ?></td>
                  </a>
              <td align="center" ><?php echo $row['commentaire']; ?></td>
          </tr>
     <?php
      }
      ?>
      </tbody>
  </table>

  <?php
   } ?>

  <?php
  if($res && $num): //Show options..
      echo sprintf('<div>&nbsp;'.__('Page').': %s &nbsp; <a class="no-pjax"
              href="orgs.php?a=export&qh=%s&organ=%s">'.__('Export').'</a></div>',
              $pageNav->getPageLinks(),
              $qhash,
              $org->ht['name']);
  endif;
  ?>
  </div>
</div>
<?php
include(CLIENTINC_DIR.'footer.inc.php');
?>
