<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
session_start();

$_SESSION['design'] = "avea";  // on demarre la session et on save le design dans la variable

$logo_perso = $_SESSION['design'];  

require('client.inc.php');
$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');




if($_SESSION['design'] == 'avea')
{
//    echo "-----------------------AVEA-----------------------------";
}


?>




<?php
//var_dump($_SESSION['test']);
  //echo('Location: '.ASSETS_PATH.'images/logo'.$logo_perso.'.png');
// uncomment ici pour revenir à l'original
//$eteind = '';
//if($_REQUEST["caroussel"] == "1"){
//    $eteind = "switchOff";
//}

// pour éteindre toutes les parties indésirables, 
// on fait de la place pour le caroussel
$eteind = "switchOff";
?>

<div id="landing_page" class="<?php echo $eteind; ?>"> 

    <?php
    if($cfg && ($page = $cfg->getLandingPage()))
        echo $page->getBodyWithImages();
    else
        echo  '<h1 class="'.$eteind.'">'.__('Welcome to the Support Center').'</h1>';
    ?>
    <div id="new_ticket" class="pull-left">
        <h3><?php echo __('Open a New Ticket');?></h3>
        <br>
        <div><?php echo __('Please provide as much detail as possible so we can best assist you. To update a previously submitted ticket, please login.');?></div>
    </div>

    <div id="check_status" class="pull-right">
        <h3><?php echo __('Check Ticket Status');?></h3>
        <br>
        <div><?php echo __('We provide archives and history of all your current and past support requests complete with responses.');?></div>
    </div>

    <div class="clear"></div>
    <div class="front-page-button pull-left">
        <p>
            <a href="open.php" class="green button"><?php echo __('Open a New Ticket');?></a>
        </p>
    </div>
    <div class="front-page-button pull-right <?php echo $eteind; ?>">
        <p>
            <a href="view.php" class="blue button"><?php echo __('Check Ticket Status');?></a>
        </p>
    </div>
</div>
<div class="clear"></div>

<?php
if($cfg && $cfg->isKnowledgebaseEnabled()){
    //FIXME: provide ability to feature or select random FAQs ??
?>
<p class="<?php echo $eteind; ?>"><?php echo sprintf(
    __('Be sure to browse our %s before opening a ticket'),
    sprintf('<a href="kb/index.php">%s</a>',
        __('Frequently Asked Questions (FAQs)')
    )); ?></p>

<?php
if($_REQUEST["caroussel"] != ""){
?>
    <p id="optimis_link" class="<?php echo $eteind; ?>">plus d'informations sur notre site: 
        <a href="http://www.groupeoptimis.fr/vdevsite/" target="_blank">www.groupeoptimis.fr</a>
    </p>
<?php
} 
?>




<?php
// uncomment ici pour revenir à l'original
//if($_REQUEST["caroussel"] == 1){
?>
    <div class="clear"></div>
    
    <!--si c'est Avea-->
    <div id="presentation_<?php echo $_SESSION['design']; ?>">
            <h1 class="title_<?php echo $_SESSION['design']; ?>">Support</h1>
            <ul class="ul_text_support_<?php echo $_SESSION['design']; ?>">
                <li class="li_text_support_<?php echo $_SESSION['design']; ?>">
                   <div class="li_contener_<?php echo $_SESSION['design']; ?>">
                        <p class="p_<?php echo $_SESSION['design']; ?>">
                    L'équipe Avea met à votre disposition cet espace
                        </p>
                        <ul class="ul_<?php echo $_SESSION['design']; ?>">
                            <li>Faites vos demandes de support en créant un nouveau ticket</li>
                            <li>Accédez à la FAQ de la base de connaissances</li>
                            <li>Suivez en temps réel le statut de vos tickets</li>
                            <li>Gardez l'historique de vos appels techniques</li>
                        </ul>
                    </div>
                </li>
                <li class="li_text_support_<?php echo $_SESSION['design']; ?>">
                    <div class="li_contener_<?php echo $_SESSION['design']; ?>">
                        <a href="open.php" class="button_<?php echo $_SESSION['design']; ?>"><?php echo __('Open a New Ticket'); ?></a>
                    </div>
                </li>
            </ul>
            <h1 class="title_<?php echo $_SESSION['design']; ?>">Actualités</h1>    
        </div>
    
    <!--si ce n'est pas Avea-->
    <div class="front-page-button pull-left <?php echo $eteind; ?>">
        <p>
            <a href="open.php" class="green button"><?php echo __('Open a New Ticket');?></a>
        </p>
    </div>
    <div class="clear"></div>
    
<div id="container_caroussel">
    <div class="your-class">
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/buzi.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/avea.png'; ?>"/></div>
        <div><img src="<?php echo ROOT_PATH.'images/caroussel/chaine_valeur_erp.png'; ?>"/></div>
    </div>
</div>
<?php
// uncomment ici pour revenir à l'original
//} 
?>
    
</div>

<?php
} ?>
<?php 
require(CLIENTINC_DIR.'footer.inc.php'); 
?>
